Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Blobby Volley 2
Upstream-Contact: Daniel Knobe <daniel-knobe@web.de>
Source: https://sourceforge.net/projects/blobby/
Files-Excluded: deps/lua deps/tinyxml

Files: *
Copyright: 2006, Jonathan Sieber <jonathan_sieber@yahoo.de>
           2006,2022, Daniel Knobe <daniel-knobe@web.de>
           2022, Erik Schultheis <erik-schultheis@freenet.de>
License: GPL-2+

Files: src/raknet/*
Copyright: 2003, Rakkarsoft LLC and Kevin Jenkins.
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the <organization> nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: linux/blobby.appdata.xml
Copyright: Blobby development team
License: CC0-1.0
 On Debian systems, the complete text of the Creative Commons
 CC0-1.0 license can be found in `/usr/share/common-licenses/CC0-1.0'.

Files: debian/*
Copyright: 2009-2012, Felix Geyer <debfx-pkg@fobos.de>
License: GPL-2+

License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
